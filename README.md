# Car Management API 

Car Management API (Backend Only)

- Superadmin

```
email = superadmin@gmail.com
password = 12345678
```

## Features

- ### Account Management

```
  Create Account
  Read Account
  Read Account by id
  Update Account
  Delete Account (Soft Delete)
```

- ### Car Management

```
  Create Car
  Read list of Cars
  Read car by id
  Update Car
  Delete Car (Soft Delete)
```

- ### Login 

```
  Login with Token
```

## How to Run (Installation)

```
yarn install
Edit file config.js with your postgre username and password
yarn sequelize-cli db:create
yarn migrate
yarn sequelize-cli db:seed:all
yarn start
```

## Endpoints

```
- /docs = API Documentation & testing
- /api/v1/login = Login
- /api/v1/admins = Post & Get Admin Account
- /api/v1/admins/:id = Get by id, Put, Delete(Soft Delete) Admin Account
- /api/v1/cars = Post & Get Car Data
- /api/v1/cars/:id = Get by id, Put, Delete(Soft Delete) Car Data
- /api/v1/register = Post Member Account
- /api/v1/profile = Get current login account
```

## Entity Relationship Diagram

<img src="erd.png">
